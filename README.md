# deepCounterfactuals
![image info](./pictures/image.png)

_Currently under construction, please contact Michel Besserve for any code request._

This website will host implementations for the paper
## Counterfactuals uncover the modular structure of deep generative models 
by Michel Besserve, Arash Mehrjou, Rémy Sun and Bernhard Schölkopf
https://arxiv.org/abs/1812.03253

