#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Tools for influence analysis

"""
import numpy as np
import sys
sys.path.append('..')
import os
from sdr_generators.helper_functions.utils import print_no_newline

def compInflMapsOld(sess,genOut,feedDict,initFeed,estimand='absDiff',featSp='pix') :
    """
    Compute influence maps (influence on final output image) of changing each channel activation maps (output) of a given layer.

    Parameters:
    -----------

    sess: Tensorflow session.
    The Tensorflow session in which the network is loaded.

    genOut: Tensorflow tensor.
    Tensor holding the  generative network output (the image typically).

    feedDict: 4D Tensorflow tensor. (samples,height,width,channels)
    The tensorflow tensor (graph node) holding the convolutional activation maps for the selected layer.

    initFeed: 4D numpy array. (samples,height,width,channels)
    Array of the activation values of the convolutional activation maps for the selected layer (should be the same one as for feedDict).

    estimand: string.
    Metric tracked over the output difference for the influence map. Can be 'absDiff','squareDiff' or 'var'

    Returns:
    --------

    inflMap: 4D numpy array. (channels,img_height,img_width,rgb_channels).
    Computed influence maps of changes at different channels on final output image.
    """
    dinit = {feedDict: np.copy(initFeed)}
    
    
    img = sess.run(genOut, feed_dict=dinit)
    img = featMap(img,featSp=featSp)
    # feel matrix storing responses for all channels
    inflMap = np.tile(np.copy(img[0,:,:,:]),(initFeed.shape[-1],1,1,1))
    dIter = {}
    for cutLayer in range(initFeed.shape[-1]) :
        print(".",end='')
        print("{}/{}".format(cutLayer, initFeed.shape[-1]))
        post_activations = np.copy(initFeed)
        #exchanging feature between samples
        post_activations[:,:,:,cutLayer] = post_activations[::-1,:,:,cutLayer] 
        dIter.update({feedDict: post_activations})
        
        #img2 = sess.run(genOut, feed_dict={feedDict: post_activations})
        img2 = sess.run(genOut, feed_dict=dIter)
        img2 = featMap(img2,featSp=featSp)
        if estimand == 'absDiff':
            inflMap[cutLayer,:,:,:] = np.tile(np.mean(np.abs(img2-img),axis=(0,3))[:,:,np.newaxis],(1,1,3))
        elif estimand == 'squareDiff':
            inflMap[cutLayer,:,:,:] = np.tile(np.mean(np.abs(img2-img)**2,axis=(0,3))[:,:,np.newaxis],(1,1,3))
        elif estimand == 'var':
            inflMap[cutLayer,:,:,:] = np.tile(np.var(img2-img,axis=(0,3))[:,:,np.newaxis],(1,1,3))
    print("\n")
    return inflMap


def featMap(img,featSp='pix'):

   if featSp == 'wavelet':
      import pywt
      x=pywt.wavedec2(img,'haar',axes=(1,2))
      img,sl = pywt.coeffs_to_array(x,axes=(1,2))
   
   return img
   
   
   


def compInflMapsOldLatent(sess,genOut,feedDict,initFeed,estimand='absDiff') :
    """
    Compute influence maps (influence on final output image) of changing each channel activation maps (output) of a given layer.

    Parameters:
    -----------

    sess: Tensorflow session.
    The Tensorflow session in which the network is loaded.

    genOut: Tensorflow tensor.
    Tensor holding the  generative network output (the image typically).

    feedDict: 4D Tensorflow tensor. (samples,height,width,channels)
    The tensorflow tensor (graph node) holding the convolutional activation maps for the selected layer.

    initFeed: 4D numpy array. (samples,height,width,channels)
    Array of the activation values of the convolutional activation maps for the selected layer (should be the same one as for feedDict).

    estimand: string.
    Metric tracked over the output difference for the influence map. Can be 'absDiff','squareDiff' or 'var'

    Returns:
    --------

    inflMap: 4D numpy array. (channels,img_height,img_width,rgb_channels).
    Computed influence maps of changes at different channels on final output image.
    """
    dinit = {feedDict: np.copy(initFeed)}
    
    
    img = sess.run(genOut, feed_dict=dinit)

    # feel matrix storing responses for all channels
    inflMap = np.tile(np.copy(img[0,0,0,:])[np.newaxis,:],(initFeed.shape[-1],1))
    dIter = {}
    for cutLayer in range(initFeed.shape[-1]) :
        print(".",end='')
        print("{}/{}".format(cutLayer, initFeed.shape[-1]))
        post_activations = np.copy(initFeed)
        #exchanging feature between samples
        post_activations[:,cutLayer] = post_activations[::-1,cutLayer] 
        dIter.update({feedDict: post_activations})
        
        #img2 = sess.run(genOut, feed_dict={feedDict: post_activations})
        img2 = sess.run(genOut, feed_dict=dIter)
        if estimand == 'absDiff':
            inflMap[cutLayer,:] = np.mean(np.abs(img2-img),axis=(0,1,2))
        elif estimand == 'squareDiff':
            inflMap[cutLayer,:,:,:] = np.tile(np.mean(np.abs(img2-img)**2,axis=(0,3))[:,:,np.newaxis],(1,1,3))
        elif estimand == 'var':
            inflMap[cutLayer,:,:,:] = np.tile(np.var(img2-img,axis=(0,3))[:,:,np.newaxis],(1,1,3))
    print("\n")
    return inflMap

def compInflMaps(sess,genOut,feedDict,initFeed,estimand='absDiff',skipFeedDict={},featSp='pix') :
    """
    Compute influence maps (influence on final output image) of changing each channel activation maps (output) of a given layer.

    Parameters:
    -----------

    sess: Tensorflow session.
    The Tensorflow session in which the network is loaded.

    genOut: Tensorflow tensor.
    Tensor holding the  generative network output (the image typically).

    feedDict: 4D Tensorflow tensor. (samples,height,width,channels)
    The tensorflow tensor (graph node) holding the convolutional activation maps for the selected layer.

    initFeed: 4D numpy array. (samples,height,width,channels)
    Array of the activation values of the convolutional activation maps for the selected layer (should be the same one as for feedDict).

    estimand: string.
    Metric tracked over the output difference for the influence map. Can be 'absDiff','squareDiff' or 'var'

    Returns:
    --------

    inflMap: 4D numpy array. (channels,img_height,img_width,rgb_channels).
    Computed influence maps of changes at different channels on final output image.
    """
    dinit = {feedDict: initFeed}
    
    dinit.update(skipFeedDict)
    
    img = sess.run(genOut, feed_dict=dinit)
    img = featMap(img,featSp=featSp)
    # feel matrix storing responses for all channels
    inflMap = np.tile(np.copy(img[0,:,:,:]),(initFeed.shape[-1],1,1,1))
    dIter = skipFeedDict
    for cutLayer in range(initFeed.shape[-1]) :
        print(".",end='')
        print("{}/{}".format(cutLayer, initFeed.shape[-1]))
        post_activations = np.copy(initFeed)
        #exchanging feature between samples
        post_activations[:,:,:,cutLayer] = post_activations[::-1,:,:,cutLayer] 
        dIter.update({feedDict: post_activations})
        
        #img2 = sess.run(genOut, feed_dict={feedDict: post_activations})
        img2 = sess.run(genOut, feed_dict=dIter)
        img2 = featMap(img2,featSp=featSp)
        if estimand == 'absDiff':
            inflMap[cutLayer,:,:,:] = np.tile(np.mean(np.abs(img2-img),axis=(0,3))[:,:,np.newaxis],(1,1,3))
        elif estimand == 'squareDiff':
            inflMap[cutLayer,:,:,:] = np.tile(np.mean(np.abs(img2-img)**2,axis=(0,3))[:,:,np.newaxis],(1,1,3))
        elif estimand == 'var':
            inflMap[cutLayer,:,:,:] = np.tile(np.var(img2-img,axis=(0,3))[:,:,np.newaxis],(1,1,3))
    print("\n")
    return inflMap

def compInflMapsForList(sess,genOut,feedDict,initFeed,estimand='absDiff',skipFeedDict={}) :
    """
    Compute influence maps (influence on final output image) of changing each channel activation maps (output) of a given layer.

    Parameters:
    -----------

    sess: Tensorflow session.
    The Tensorflow session in which the network is loaded.

    genOut: Tensorflow tensor.
    Tensor holding the  generative network output (the image typically).

    feedDict: 4D Tensorflow tensor. (samples,height,width,channels)
    The tensorflow tensor (graph node) holding the convolutional activation maps for the selected layer.

    initFeed: 4D numpy array. (samples,height,width,channels)
    Array of the activation values of the convolutional activation maps for the selected layer (should be the same one as for feedDict).

    estimand: string.
    Metric tracked over the output difference for the influence map. Can be 'absDiff','squareDiff' or 'var'

    Returns:
    --------

    inflMap: 4D numpy array. (channels,img_height,img_width,rgb_channels).
    Computed influence maps of changes at different channels on final output image.
    """
    if not isinstance(initFeed, list):
        initFeed = [initFeed]
        feedDict = [feedDict]
    
    dinit = {key: value for (key,value) in zip(feedDict, initFeed)}
    
    dinit.update(skipFeedDict)
    
    img = sess.run(genOut, feed_dict=dinit)

    # feel matrix storing responses for all channels
    inflMap = np.tile(np.copy(img[0,:,:,:]),(initFeed.shape[-1],1,1,1))
    dIter = skipFeedDict
    for cutLayer in range(initFeed.shape[-1]) :
        print(".",end='')
        posAct = []
        for kTens in range(len(feedDict)):
            post_activations = np.copy(initFeed[kTens])
            #exchanging feature between samples
            post_activations[:,:,:,cutLayer[kTens]] = post_activations[::-1,:,:,cutLayer[kTens]]
            posAct.append(post_activations)
        dIter.update({key: value for (key,value) in zip(feedDict, posAct)})  
        #img2 = sess.run(genOut, feed_dict={feedDict: post_activations})
        img2 = sess.run(genOut, feed_dict=dIter)
        if estimand == 'absDiff':
            inflMap[cutLayer,:,:,:] = np.tile(np.mean(np.abs(img2-img),axis=(0,3))[:,:,np.newaxis],(1,1,3))
        elif estimand == 'squareDiff':
            inflMap[cutLayer,:,:,:] = np.tile(np.mean(np.abs(img2-img)**2,axis=(0,3))[:,:,np.newaxis],(1,1,3))
        elif estimand == 'var':
            inflMap[cutLayer,:,:,:] = np.tile(np.var(img2-img,axis=(0,3))[:,:,np.newaxis],(1,1,3))
    print("\n")
    return inflMap


def compFiltInflMaps(sess,genOut,feedDict,initFeed,weightDict) :
    """
    Compute influence maps (influence on final output image) of changing each channel filter of a given layer.

    Parameters:
    -----------

    sess: Tensorflow session.
    The Tensorflow session in which the network is loaded.

    genOut: Tensorflow tensor.
    Tensor holding the  generative network output (the image typically).

    feedDict: 4D Tensorflow tensor. (samples,height,width,channels)
    The tensorflow tensor (graph node) holding the convolutional activation maps for the selected layer.

    initFeed: 4D numpy array. (samples,height,width,channels)
    Array of the activation values of the convolutional activation maps for the selected layer (should be the same one as for feedDict).

    weightDict: 4D numpy array. (kernel_height,kernel_width,out_channels,in_channels).
    Array holding kernel weight values for the selected layer (same one as for feedDict and feedInit)

    Returns:
    --------

    inflMap: 4D numpy array. (channels,img_height,img_width,rgb_channels).
    Computed influence maps of changes at different channels on final output image.
    """
    """
    Compute influence maps for filters
    """
    weights = sess.run(weightDict)
    img = sess.run(genOut, feed_dict={feedDict: initFeed})
    inflMap = np.tile(np.copy(img[np.newaxis,0,:,:,:]),(weights.shape[2],weights.shape[3],1,1,1))
    for kupchan in range(weights.shape[3]) :
        for kdownchan in range(weights.shape[2]) :
            perWeights = np.copy(weights)
            perWeights[:,:,kdownchan,kupchan] = weights[:,:,kdownchan,kupchan] + .01*np.random.randn(weights.shape[0],weights.shape[1])
            weightDict.load(perWeights)
            img2 = sess.run(genOut, feed_dict={feedDict: initFeed})
            inflMap[kdownchan,kupchan,:,:,:] = np.tile(np.mean(np.abs(img2-img),axis=(0,3))[:,:,np.newaxis],(1,1,3))
    return inflMap

def clustInfluence(patterns, clustMethod = 'NMF', extractPercentile = True, Nclust=5, filtSize = 6, selectPureClusters = True, purityThres = .5) :
    """
    Clustering influence maps into a certain number of clusters according to some clustering method.

    Parameters:
    -----------

    patterns: list of 4D numpy array containing patterns. (nb_patterns,xaxis,yaxis, color).
    List of influence maps for each layer (one array per layer considered)

    clustMethod: string. (default 'NMF').
    The clustering method used to find modules, 'NMF' means NMF, anything else is kmeans.

    extractPercentile: bool. (default True).
    If True preprocess maps by thresholding to the 75th percentile.

    Nclust: int. (default 5).
    Number of clusters computed by the algorithms.

    filtSize: int. (default 6).Size of filter 
    Number of pixels of the side of moving average filter

    selectPureClusters: bool. (default True)
    If yes, remove cluster elements deemed not pure enough.

    purityThres: float. (default 0.5)
    Threshold used to decide purity of cluster elements.

    Returns:
    --------

    clustLabelsByLayer: list of arrays.
    Contains cluster labels, one for each layer.
    meanPattsR: 4D numpy array. (NClust,xaxis,yaxis,rgb_channels).
    Cluster centers for each the extracted pattern clusters.

    purity: Array.
    Purity levels of each pattern for their assigned cluster assignment.
    """
    # convert list of arrays across layers to single array with channels of all layers on the first dimension
    X = np.concatenate(np.copy(patterns),axis=(0)) 
    # extract features and preprocess
    #keep only one color (gray level coding)
    Xpreproc = X[:,:,:,0]
    Xpreproc = np.real(np.fft.ifft2(np.fft.fft2(Xpreproc, axes=(1,2))*np.fft.fft2(np.ones([1,filtSize,filtSize]),axes=(1,2),s=[Xpreproc.shape[1],Xpreproc.shape[2]]),axes=(1,2)))
    #np.put(Xpreproc,(Xpreproc<0),0)
    Xpreproc = np.clip(Xpreproc,0,None)
    # use median
    #Xpreproc = np.less(2*np.median(Xpreproc,axis=(1,2),keepdims=True),Xpreproc)

    if extractPercentile :
        #use percentile
        Xpreproc = np.less(np.percentile(Xpreproc,75,axis=(1,2),keepdims=True),Xpreproc)

    # flatten features
    Xfeats = np.reshape(Xpreproc,(Xpreproc.shape[0],-1))

    if clustMethod == 'NMF' :
        from sklearn.decomposition import NMF
        model = NMF(n_components=Nclust, init='random', random_state=0)
        W = model.fit_transform(Xfeats)
        H = model.components_
        meanPatts = H
        clustLabels = np.argmax(W,axis=1)
        
        # compute purity ratios
        if selectPureClusters :
            purity = np.zeros([W.shape[0]])
            for mapIdx in range(W.shape[0]) :
                purity[mapIdx] = (np.sum((W[mapIdx,clustLabels[mapIdx]]*H[clustLabels[mapIdx],:])**2)/
                                  np.sum(Xfeats[mapIdx,:]**2))
                
            clustLabels[purity<purityThres]=[-1]*np.sum(purity<purityThres)
    
    else: # use kmeans...
        from sklearn.cluster import KMeans
        kmeans = KMeans(n_clusters=Nclust, random_state=0).fit(Xfeats)
        meanPatts = kmeans.cluster_centers_
        clustLabels = kmeans.labels_
        purity = []

    # extract labels of each layer
    clustLabelsByLayer = np.split(clustLabels,np.cumsum([x.shape[0] for x in patterns][:-1]))

    # plot clustering result
    meanPattsR = np.reshape(meanPatts,(Nclust,patterns[0].shape[1],patterns[0].shape[2]))
    # add again colors (gray levels)
    meanPattsR = np.tile(meanPattsR[:,:,:,np.newaxis],(1,1,1,3))
    return clustLabelsByLayer, meanPattsR, purity


def clustConsist(patterns,filtSizList = [2,4,6],clusTypeList = ['NMF','kmeans'],nClustList=range(2,10),nTrial=20, path_to_save_intermediates=None) :
    """
    Computation of consistency across parameters
    """
    import itertools as it
    clustQual = [[[[] for k in clusTypeList] for l in filtSizList] for m in range(nTrial)]
    cosSim = [[[[] for k in clusTypeList] for l in filtSizList] for m in range(nTrial)]
#    print(cosSim)
    for kTrial in range(len(clustQual)) :
        for kfilt, filtSize in enumerate(filtSizList) :
            for kclust, clusType in enumerate(clusTypeList) :
                for clust_ind, Nclust in enumerate(nClustList) :
                    print_no_newline("Trial: {}/{}, Filt: {}/{}, ClustType: {}/{}, ClustNum: {}/{}\r".format(kTrial + 1, len(clustQual), kfilt + 1, len(filtSizList), kclust + 1, len(clusTypeList), clust_ind + 1, len(nClustList)))
                    # cut in 3 parts

                    partIdx = [np.random.randint(0,3,[len(patt)]) for patt in patterns]
                    
                    # train with common part 1
                    clustLabelsByLayer, meanPattsR, _ = clustInfluence([patt[idx>0] for patt,idx in zip(patterns,partIdx)], clustMethod=clusType, extractPercentile=True, Nclust=Nclust, filtSize=filtSize, selectPureClusters=True, purityThres=.1)
                    clustLabelsByLayer2, meanPattsR2, _ = clustInfluence([patt[idx<2] for patt,idx in zip(patterns,partIdx)], clustMethod=clusType, extractPercentile=True, Nclust=Nclust, filtSize=filtSize, selectPureClusters=True, purityThres=.1)
                    centerPart = np.concatenate([clustLab[idx[idx>0]==1] for clustLab, idx in zip(clustLabelsByLayer,partIdx)])
                    centerPart2 = np.concatenate([clustLab[idx[idx < 2] == 1] for clustLab, idx in zip(clustLabelsByLayer2, partIdx)])
                    # enforce not clustered points lead to a mismatch
                    centerPart2[centerPart2==-1]=-2
                    # find the right match between clusters
                    clustOverlap = []
                    optVal = -1
    
                    for perm in it.permutations(range(Nclust)) :
                        f = lambda x: perm[x]
                        clustOverlap.append(np.mean(np.equal(centerPart,np.fromiter(map(f,centerPart2),int))))
                        if clustOverlap[-1]>optVal: permOpt, optVal = perm, clustOverlap[-1]
                    clustQual[kTrial][kfilt][kclust].append(optVal)
                    meanPattsR2mapped = 0*meanPattsR2
                    meanPattsR2mapped[permOpt,:,:,:] = meanPattsR2
                    #normalize vectors of patterns
                    V1 = meanPattsR/np.sqrt(np.sum(meanPattsR**2,axis=(1,2,3),keepdims=True))
                    V2 = meanPattsR2mapped/np.sqrt(np.sum(meanPattsR2mapped**2,axis=(1,2,3),keepdims=True))
                    # compute cosine similarity
                    cosSim[kTrial][kfilt][kclust].append(np.sum(V1*V2))
#                    print(cosSim)
#        print(np.array(cosSim).shape)
#        print(np.array(clustQual).shape)
#        print(np.array(nClustList).shape)
#        if path_to_save_intermediates is not None:
#             if not os.path.exists(path_to_save_intermediates):
#                 print('New directry has been created')
#                 os.makedirs(path_to_save_intermediates)
#             clstQual = np.array(clustQual)
#             if kTrial > 0:
#                 csSim = np.array(cosSim)
#                 print("csSim:")
#                 print(csSim)
#                 print("Type")
#                 print(type(csSim))
#                 print("clstQual:")
#                 print(clstQual)
#                 print("nClustList")
#                 print(nClustList)
#                 print("Divisor")
#                 print(np.array(nClustList)[np.newaxis,np.newaxis,np.newaxis,:])
#                 csSim =  csSim/np.array(nClustList)[np.newaxis,np.newaxis,np.newaxis,:]
#             else:
#                 csSim = -1
#             np.savez(os.path.join(path_to_save_intermediates, 'Trial_{}'.format(kTrial)), clstQual, csSim)
    clstQual = np.array(clustQual)
    csSim = np.array(cosSim)
    csSim =  csSim/np.array(nClustList)[np.newaxis,np.newaxis,np.newaxis,:]
    return clstQual, csSim



def clustSizInfl(patterns,sess,net_g,intermediate_act,activations,filtSizList = [2,4,6],clusTypeList = ['NMF','kmeans'],clustSizeList=range(2,10),nTrial=20,randomize = False) :
    """
    Compute the effect on the size of clsuter on their individual influence for hybridization
    """
    outSize = net_g.outputs.get_shape().as_list()
    
    clustInfl = [[[[]for l in clustSizeList] for k in clusTypeList]  for m in range(nTrial)] 
    clustSiz = [[[[]for l in clustSizeList] for k in clusTypeList]  for m in range(nTrial)] 
    for kTrial in range(nTrial) :
        filtSize = filtSizList[-1]
        for kclust, clusType in enumerate(clusTypeList) :
            for kNumClust, Nclust in enumerate(clustSizeList) :
                clustLabelsByLayer, meanPattsR, _ = clustInfluence(patterns,clustMethod=clusType,extractPercentile=True, Nclust=Nclust, filtSize=filtSize, selectPureClusters=True,purityThres=.1)
                if randomize :#random permutation of labels per layer
                    clustLabelsByLayer = [np.random.permutation(lab) for lab in clustLabelsByLayer]
                    
                for selLayer in range(3) :
                    # build hybrid examples by intervening on this layer
                    sizClust = []
                    hybridExples = np.zeros([Nclust,3,outSize.shape[0],outSize.shape[1],outSize.shape[2],outSize.shape[3]])
                    for clustIdx in range(Nclust) :
                        cutLayer = np.where(clustLabelsByLayer[selLayer]==clustIdx)[0]
                        hybridExples[clustIdx,:,:,:,:,:] = hybridization(sess,net_g.outputs,intermediate_act[selLayer],activations[selLayer],cutLayer)
                        sizClust.append(len(cutLayer))
                    clustInfl[kTrial][kclust][kNumClust].append( np.mean(np.abs(hybridExples[:,0,:,:,:,:]-hybridExples[:,1,:,:,:,:]),axis=(1,2,3,4)))
                    clustSiz[kTrial][kclust][kNumClust].append(np.array(sizClust))

    return clustInfl, clustSiz

def hybridization(sess,genOut,feedDict,initFeed,cutLayer,skipFeedDict = {}) :
    """
    Create hybrid sample at specified layer by swapping activation maps (through reversal of batch order) of certain activation maps.

    Parameters:
    -----------

    sess: Tensorflow session.
    The Tensorflow session in which the network is loaded.

    genOut: Tensorflow tensor.
    Tensor holding the  generative network output (the image typically).

    feedDict: 4D Tensorflow tensor (samples,height,width,channels), or list of such tensors
    The tensorflow tensor (graph node) holding the convolutional activation maps for the selected layer.

    initFeed: 4D numpy array. (samples,height,width,channels)
    Array of the activation values of the convolutional activation maps for the selected layer (should be the same one as for feedDict).

    cutLayer: numpy array.
    Array containing the indices of channels which we are using for intervention.
    
    skipFeedDict: dictionary 
    Contains tensor: values of skip connections

    Returns:
    --------

    hybridExples: 4D numpy array. (3,img_height,img_width,rgb_channels). [0,:,:,:] is original 1, [1,:,:,:] is hybrid, [2,:,:,:] is original 2
    Array containing a hybridized image along with the two original images it merged.
    """
    if not isinstance(initFeed, list):
        initFeed = [initFeed]
        feedDict = [feedDict]
        cutLayer = [cutLayer]
    dinit = {key: value for (key,value) in zip(feedDict, initFeed)}
    
    dTot = skipFeedDict
    dTot.update(dinit)
    img = sess.run(genOut, feed_dict=dTot)
    hybridExples = np.zeros([3,img.shape[0],img.shape[1],img.shape[2],img.shape[3]])
    posAct = []
    for kTens in range(len(feedDict)):
        post_activations = np.copy(initFeed[kTens])
        #exchanging feature between samples
        post_activations[:,:,:,cutLayer[kTens]] = post_activations[::-1,:,:,cutLayer[kTens]]
        posAct.append(post_activations)
#    post_activations = np.copy(initFeed)
#    #cutLayer = np.where(clustLabelsByLayer[selLayer]==clustIdx)[0]
#    #exchanging feature between samples
#    post_activations[:,:,:,cutLayer] = post_activations[::-1,:,:,cutLayer] 
    #dTot.update({feedDict: post_activations})
    dTot.update({key: value for (key,value) in zip(feedDict, posAct)})  
    img2 = sess.run(genOut, feed_dict=dTot)
    hybridExples[0,:,:,:,:] = img[:,:,:,:]
    hybridExples[2,:,:,:,:] = img[::-1,:,:,:]
    hybridExples[1,:,:,:,:] = img2[:,:,:,:]    
    return hybridExples

def hybridizationfree(sess,genOut,feedDict,initFeed1,initFeed2,cutLayer,skipFeedDict={}) :
    """
    Create hybrid sample at specified layer by swapping activation maps (between two sets of activation maps values) of certain activation maps.

    Parameters:
    -----------

    sess: Tensorflow session.
    The Tensorflow session in which the network is loaded.

    genOut: Tensorflow tensor.
    Tensor holding the  generative network output (the image typically).

    feedDict: 4D Tensorflow tensor (samples,height,width,channels), or list of such tensors.
    The tensorflow tensor (graph node) holding the convolutional activation maps for the selected layer.

    initFeed1: 4D numpy array (samples,height,width,channels) or list of such tensors if feedDict is a list
    Array of the activation values of the convolutional activation maps for the selected layer (should be the same one as for feedDict) for Original 1.

    initFeed2: 4D numpy array (samples,height,width,channels) or list of such tensors if feedDict is a list
    Array of the activation values of the convolutional activation maps for the selected layer (should be the same one as for feedDict) for Original 2.


    cutLayer: numpy array or list of those if feedDict is a list
    Array containing the indices of channels which we are using for intervention.
    
    skipFeedDict: dictionary 
    Contains tensor: values of skip connections

    Returns:
    --------

    hybridExples: 4D numpy array. (3,img_height,img_width,rgb_channels). [0,:,:,:] is original 1, [1,:,:,:] is hybrid, [2,:,:,:] is original 2
    Array containing a hybridized image along with the two original images it merged.
    """
    if not isinstance(feedDict, list):
        initFeed1 = [initFeed1]
        initFeed2 = [initFeed2]
        feedDict = [feedDict]
        cutLayer = [cutLayer]
        print("inputs converted to lists")
    
    dinit = {key: value for (key,value) in zip(feedDict, initFeed1)}
    dTot = skipFeedDict
    dTot.update(dinit)
    img1 = sess.run(genOut, feed_dict=dTot)
    
    dinit = {key: value for (key,value) in zip(feedDict, initFeed2)}
    dTot.update(dinit)
    img2 = sess.run(genOut, feed_dict=dTot)
    hybridExples = np.zeros([3,img1.shape[0],img1.shape[1],img1.shape[2],img1.shape[3]])
#    post_activations = np.copy(initFeed1)
#    post_activationsPert = np.copy(initFeed2)
    posAct = []
    for kTens in range(len(feedDict)):
        post_activations = np.copy(initFeed1[kTens])
        post_activationsPert = np.copy(initFeed2[kTens])
        #exchanging feature between samples
        post_activations[:,:,:,cutLayer[kTens]] = post_activationsPert[:,:,:,cutLayer[kTens]] 
        posAct.append(post_activations)
    #cutLayer = np.where(clustLabelsByLayer[selLayer]==clustIdx)[0]
    #exchanging feature between samples
    dTot.update({key: value for (key,value) in zip(feedDict, posAct)})  
    imgPert = sess.run(genOut, feed_dict=dTot)
    hybridExples[0,:,:,:,:] = img1[:,:,:,:]
    hybridExples[2,:,:,:,:] = img2[:,:,:,:]
    hybridExples[1,:,:,:,:] = imgPert[:,:,:,:]
    # print("img1")
    # print(hybridExples[1,:,:,:,0])
    # print("img2")
    # print(hybridExples[0,:,:,:,0])
    return hybridExples

def hybridizationfreeIter(sess, genOut, feedDict, initFeed1, initFeed2, cutLayer, skipFeedDict={}, skipFeedDictRef={}, alpha=1, beta=0, alphaDecay = "no") :
    """
    Create hybrid sample at specified layers by swapping activation maps (between two sets of activation maps values) of certain activation maps. 
    This procedure is iterated across successive layers: next layer values are computed from previous layer before sample from initFeed2 at this layer is injected. 
    To control the strength of the influence, there is a parameter alpha in the function (max strength for alpha =1), this controls the respective contribution of intevention with respect to original values of the layer. The effect of alpha can be decayed across successive layer using the alphaDecay command: "no" means not decay, "arithm" means alpha gets divided by layer depth, "geom" mean it decays as a power of the layer depth. A parameter beta also adjusts the balance between the influence of skipFeedDict and skipFeedDictRef for the dictionary of additional tensors that have no modules specified (typically skip connections).

    Parameters:
    -----------

    sess: Tensorflow session.
    The Tensorflow session in which the network is loaded.

    genOut: Tensorflow tensor.
    Tensor holding the  generative network output (the image typically).

    feedDict: a list (across layer) of list (across tensors, typically one conv layer and one skip connection) of 4D Tensorflow tensor. (samples,height,width,channels)
    The tensorflow tensor (graph node) holding the convolutional activation maps for the selected layer.

    initFeed1:  a list (across layer) of list (across tensors, typically one conv layer and one skip connection) of 4D numpy array for feedDict tensors. (samples,height,width,channels)
    Array of the activation values of the convolutional activation maps for the selected layer (should be the same one as for feedDict) for Original 1.

    initFeed2: same as initFeed1 for another batch. (samples,height,width,channels)
    Array of the activation values of the convolutional activation maps for the selected layer (should be the same one as for feedDict) for Original 2.

    cutLayer: a list (across layer) of list (across tensors, typically one conv layer and one skip connection) of
    arrays containing the indices of channels which we are using for intervention.
    
    skipFeedDict: dictionary or list of dictionaries
    Contains tensor: values of skip connections to apply at each intervention ( this corresponds typically to values of the second sample, related to initFeed2, especially in case skipFeedDictRef is also provided)
    
    skipFeedDictRef: dictionary or list of dictionaries
    Contains tensor: reference values of skip connections to apply at each intervention corresponding to first sample (related to initFeed1)

    alpha: scalar in [0,1], default 1, controls the strenght of the (soft) intervention on values indexed by cutLayer
    
    alphaDecay:  String. "no" means not decay, "arithm" means alpha gets divided by layer depth, "geom" mean it decays as a power of the layer depth. 

    beta: scalar in [0,1], default 0, controls the balance of the (soft) intervention between values of skip connections in skipFeedDict ( large for beta small) and skipFeedDictRef (large for beta large).
    
    Returns:
    --------

    hybridExples: 4D numpy array. (3,img_height,img_width,rgb_channels). [0,:,:,:] is original 1, [1,:,:,:] is hybrid, [2,:,:,:] is original 2
    Array containing a hybridized image along with the two original images it merged.
    """

    if not isinstance(initFeed2, list):
        initFeed1 = [initFeed1]
        initFeed2 = [initFeed2]
        feedDict = [feedDict]
        cutLayer = [cutLayer]
    
    if not isinstance(initFeed2[0], list):
        initFeed1 = [initFeed1]
        initFeed2 = [initFeed2]
        feedDict = [feedDict]
        cutLayer = [cutLayer]
        
    if not isinstance(skipFeedDict, list):
        skipFeedDict = [skipFeedDict]*len(initFeed2)
        skipFeedDictRef = [skipFeedDictRef]*len(initFeed2)
    if alphaDecay not in ["no","geom","arithm"]:
        raise ValueError("invalid value for alphaDecay")
        
    dinit = {key: value for (key,value) in zip(feedDict[0], initFeed1[0])}
    dTot = skipFeedDictRef[0]
    dTot.update(dinit)
    img1 = sess.run(genOut, feed_dict=dTot)
    
    dinit = {key: value for (key,value) in zip(feedDict[0], initFeed2[0])}
    
    # NOTE: surprizing behavior here: the dictonary needs to be reset, and update in not enough???
    dTot = skipFeedDict[0]
    
    dTot.update(dinit)
    img2 = sess.run(genOut, feed_dict=dTot)
    hybridExples = np.zeros([3,img1.shape[0],img1.shape[1],img1.shape[2],img1.shape[3]])
    #    post_activations = np.copy(initFeed1)
    #    post_activationsPert = np.copy(initFeed2)
    for klay in range(len(initFeed2)):
        # this solution does nto work (Tensor not iterable?)
        #dTot = {key: (1-beta)*skipFeedDict[klay][key]+beta*value for key,value in skipFeedDictRef[klay]}
        dTot = {key: (1-beta)*value+beta*value2 for (key,value), (key2,value2) in zip(skipFeedDict[klay].items(),skipFeedDictRef[klay].items())}
        
        #dTot = skipMergeDict[klay]
        posAct = []
        for kTens in range(len(feedDict[klay])):
            if klay ==0:
                post_activations = np.copy(initFeed1[klay][kTens])
                post_activationsPert = np.copy(initFeed2[klay][kTens])
            else:
                post_activations = np.copy(resLay[kTens])
                post_activationsPert = np.copy(initFeed2[klay][kTens])
        #exchanging feature between samples
        # line below is an older version: will not have an effect using a single layer, default alpha would have been 0
#            post_activations[:,:,:,cutLayer[klay][kTens]] = alpha**klay *post_activations[:,:,:,cutLayer[klay][kTens]]+(1-alpha**klay)*post_activationsPert[:,:,:,cutLayer[klay][kTens]]
# now defualt alpha is one

            if alphaDecay == "no":
                post_activations[:,:,:,cutLayer[klay][kTens]] = (1-alpha) *post_activations[:,:,:,cutLayer[klay][kTens]] + alpha*post_activationsPert[:,:,:,cutLayer[klay][kTens]] 
            elif alphaDecay == "geom":
                post_activations[:,:,:,cutLayer[klay][kTens]] = (1-alpha**klay) *post_activations[:,:,:,cutLayer[klay][kTens]] + (alpha**klay)*post_activationsPert[:,:,:,cutLayer[klay][kTens]] 
            elif alphaDecay == "arithm":
                post_activations[:,:,:,cutLayer[klay][kTens]] = (1-alpha/(klay+1)) *post_activations[:,:,:,cutLayer[klay][kTens]] + (alpha/(klay+1))*post_activationsPert[:,:,:,cutLayer[klay][kTens]] 
            posAct.append(post_activations)
            #cutLayer = np.where(clustLabelsByLayer[selLayer]==clustIdx)[0]
            #exchanging feature between samples
        dTot.update({key: value for (key,value) in zip(feedDict[klay], posAct)}) 
        if klay < len(initFeed2)-1:
            resLay = sess.run(feedDict[klay+1], feed_dict=dTot)
        else:
            imgPert = sess.run(genOut, feed_dict=dTot)
    
    hybridExples[0,:,:,:,:] = img1[:,:,:,:]
    hybridExples[2,:,:,:,:] = img2[:,:,:,:]
    hybridExples[1,:,:,:,:] = imgPert[:,:,:,:] 
    return hybridExples





def hybridationStats(hybridExples,clustMask) :
    """
    Compute set of statistics to evaluate the quality of hybridation based on masking the image.

    Parameters:
    -----------

    hybridExples: 4D numpy array. (3,img_height,img_width,rgb_channels). [0,:,:,:] is original 1, [1,:,:,:] is hybrid, [2,:,:,:] is original 2
    Array containing a hybridized image along with the two original images it merged.

    Returns:
    --------

    errOrg,errMod,errOrgI,errModI,ratioOrg,ratioMod,ratioOrgI,ratioModI: errors and error ratios between the two references
    """

    if len(hybridExples.shape)==4 :
        # add sample axis
        hybridExples = hybridExples[:,np.newaxis,:,:,:]
    elif len(hybridExples.shape)!=5 :
        raise NameError('wrong size for first input')
    if len(clustMask.shape) == len(hybridExples.shape)-2 :
        # add color dimension
        clustMask = clustMask[:,:,:,np.newaxis]
    elif len(clustMask.shape) == len(hybridExples.shape)-3 :
        # add sample dimension
        clustMask = clustMask[np.newaxis,:,:,np.newaxis]
    # remove color multiple dimension from clustMask if present
    clustMask = clustMask[:,:,:,[0]]
    errOrg = np.mean((hybridExples[1,:,:,:,:]-hybridExples[0,:,:,:,:])[np.logical_not(np.tile(clustMask[[0]*hybridExples.shape[1],:,:,:],(1,1,1,3)))]**2,axis=(1,2,3))
    errMod = np.mean((hybridExples[1,:,:,:,:]-hybridExples[2,:,:,:,:])[np.tile(clustMask[[0]*hybridExples.shape[1],:,:,:],(1,1,1,3))]**2,axis=(1,2,3))
    
    # compute error with the converse image for reference of the above errors
    errOrgRef = np.mean((hybridExples[1,:,:,:,:]-hybridExples[0,:,:,:,:])[(np.tile(clustMask[[0]*hybridExples.shape[1],:,:,:],(1,1,1,3)))]**2,axis=(1,2,3))
    errModRef = np.mean((hybridExples[1,:,:,:,:]-hybridExples[2,:,:,:,:])[np.logical_not(np.tile(clustMask[[0]*hybridExples.shape[1],:,:,:],(1,1,1,3)))]**2,axis=(1,2,3))
    
    ratioOrg = errOrg/errModRef
    ratioMod = errMod/errOrgRef
    
    
    # same for color insensitive measures
    errOrgI = np.mean(np.mean(hybridExples[1,:,:,:,:]-hybridExples[0,:,:,:,:],axis=-1,keepdims=True)[np.logical_not(clustMask[[0]*hybridExples.shape[1],:,:,:])]**2,axis=(1,2,3))
    errModI = np.mean(np.mean(hybridExples[1,:,:,:,:]-hybridExples[2,:,:,:,:],axis=-1,keepdims=True)[clustMask[[0]*hybridExples.shape[1],:,:,:]]**2,axis=(1,2,3))
    
    errOrgIRef = np.mean(np.mean(hybridExples[1,:,:,:,:]-hybridExples[0,:,:,:,:],axis=-1,keepdims=True)[clustMask[[0]*hybridExples.shape[1],:,:,:]]**2,axis=(1,2,3))
    errModIRef = np.mean(np.mean(hybridExples[1,:,:,:,:]-hybridExples[2,:,:,:,:],axis=-1,keepdims=True)[np.logical_not(clustMask[[0]*hybridExples.shape[1],:,:,:])]**2,axis=(1,2,3))
    ratioOrgI = errOrgI/errModIRef
    ratioModI = errModI/errOrgIRef
    
    return errOrg,errMod,errOrgI,errModI,ratioOrg,ratioMod,ratioOrgI,ratioModI
